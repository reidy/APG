/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matrix.h"
#include "debug.h"

uint32_t matrix_index_of_connection (Matrix *m,
                                     uint32_t i,
                                     uint32_t j)
{
    return (m->customer_count + 1) * (i - 1) + j + 1;
}

void matrix_fill_obj_coefs (Dataset *ds, Matrix *m)
{
    uint32_t k = 1;
    m->obj_coefs = calloc (sizeof (double), m->nbr_coefs);

    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        for (size_t j = 0; j <= ds->nb_customer; j++)
        {
            m->obj_coefs[k++] = ds->a_dataset[i][j];
        }
    }
}

void matrix_fill_coefs (Dataset *ds, Matrix *m)
{
    size_t id = 1, row = 1, x_column = 1;
    m->matrix_coefs = calloc (sizeof (double), m->nbr_coefs + 1);
    m->column_of_coef = calloc (sizeof (int32_t), m->nbr_coefs + 1);
    m->row_of_coef = calloc (sizeof (int32_t), m->nbr_coefs + 1);
    m->row_bounds = calloc (sizeof (double), m->nbr_rows + 1);

    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        for (size_t j = 1; j <= ds->nb_customer; j++)
        {
            /* X_i */
            m->row_of_coef[id] = row;
            m->column_of_coef[id] = x_column;
            m->matrix_coefs[id] = 1;
            id++;

            /* Y_ij */
            m->row_of_coef[id] = row;
            m->column_of_coef[id] = matrix_index_of_connection (m, i,
                                                                j);
            m->matrix_coefs[id] = -1;
            id++;

            /* Bounds for the row */
            m->row_bounds[row++] = 0;
        }
        x_column += ds->nb_customer + 1;
    }

    for (size_t j = 1; j <= ds->nb_customer; j++)
    {
        for (size_t i = 1; i <= ds->nb_facility; i++)
        {
            /* Y_ij */
            m->row_of_coef[id] = row;
            m->column_of_coef[id] = matrix_index_of_connection (m, i, 
                                                                j);
            m->matrix_coefs[id] = 1;
            id++;
        }
        m->row_bounds[row++] = 1;
    }
}

Matrix *init_matrix (Dataset *ds)
{
    Matrix *m = calloc (sizeof (Matrix), 1);

    m->name = malloc (sizeof (int8_t) * (strlen (ds->filename) + 1));
    strcpy ((char *)m->name, ds->filename);

    m->customer_count = ds->nb_customer;

    m->nbr_rows = (ds->nb_facility + 1) * ds->nb_customer;
    m->nbr_columns = ds->nb_facility * (ds->nb_customer + 1);
    m->nbr_coefs = 3 * ds->nb_facility * ds->nb_customer;

    matrix_fill_coefs (ds, m);
    matrix_fill_obj_coefs (ds, m);

    // debug_matrix (m);

    return m;
}

void free_matrix (Matrix *m)
{
    free (m->name);
    free (m->matrix_coefs);
    free (m->column_of_coef);
    free (m->row_of_coef);
    free (m->row_bounds);
    free (m->obj_coefs);
    free (m);
}
