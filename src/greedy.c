/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "greedy.h"

#define CONNECTION_COST 0

uint32_t min_customer (Dataset *ds, bool *open, uint32_t column)
{
    uint32_t min = -1;

    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        if (ds->a_dataset[i][column] < min && open[i])
        {
            min = ds->a_dataset[i][column];
        }
    }

    return min;
}

uint32_t eval (Dataset *ds, bool *open)
{
    uint32_t cost = 0;

    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        if (open[i])
        {
            cost += ds->a_dataset[i][CONNECTION_COST];
        }
    }

    for (size_t i = 1; i <= ds->nb_customer; i++)
    {
        cost += min_customer (ds, open, i);
    }
    return cost;
}

G1Result *greedy_one (Dataset *ds)
{
    uint32_t tmp_cost, best_cost, best_factory;
    G1Result *res = calloc (sizeof (G1Result), 1);
    res->open = calloc (sizeof (bool), ds->nb_facility + 1);
    res->cost = -1;

    do
    {
        best_cost = res->cost;

        for (size_t i = 1; i <= ds->nb_facility; i++)
        {
            if (res->open[i])
            {
                continue;
            }
            res->open[i] = true;

            if ((tmp_cost = eval (ds, res->open)) < res->cost)
            {
                res->cost = tmp_cost;
                best_factory = i;
            }
            res->open[i] = false;
        }
        if (res->cost >= best_cost)
        {
            break;
        }
        res->open[best_factory] = true;

    } while (true);

    return res;
}

void greedy_one_free (G1Result *g1)
{
    free (g1->open);
    free (g1);
}
