/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lp.h"

/* Add constraint lines */
void lp_add_row (glp_prob *lp, Matrix *m)
{
    uint32_t row_index = 1;
    glp_add_rows (lp, m->nbr_rows);

    for (; row_index <= m->nbr_rows; row_index++)
    {
        char constraint_name[50];
        sprintf (constraint_name, "contrainte %d", row_index);

        glp_set_row_name (lp, row_index, constraint_name);
        glp_set_row_bnds (lp, row_index, GLP_LO,
                          m->row_bounds[row_index], 0.0);
    }
}

/* Add variables */
void lp_add_column (glp_prob *lp, Matrix *m)
{
    uint32_t column_index = 1;
    glp_add_cols (lp, m->nbr_columns);

    for (; column_index <= m->nbr_columns; column_index++)
    {
        char variable_name[50];
        sprintf (variable_name, "x_%d", column_index);

        //glp_set_col_kind (lp, column_index, GLP_BV);
        glp_set_col_name (lp, column_index, variable_name);
        glp_set_col_bnds (lp, column_index, GLP_LO, 0.0, 0.0);
        glp_set_obj_coef (lp, column_index,
                          m->obj_coefs[column_index]);
    }
}

/* Solve using a binary solver */
void lp_mip_solve (glp_prob *lp)
{
    glp_iocp *parm = malloc (sizeof (glp_iocp));

    glp_init_iocp (parm);
    parm->presolve = GLP_ON;
    parm->binarize = GLP_ON;
    glp_intopt (lp, parm);

    free (parm);
}

/* Extract the result and column values from the solved lp */
double *lp_extract_primal (glp_prob *lp, Matrix *m)
{
    double *primal = calloc (sizeof (double), m->nbr_columns + 1);

    primal[0] = glp_mip_obj_val (lp);

    for (uint32_t i = 1; i <= m->nbr_columns; i++)
    {
        primal[i] = glp_mip_col_val (lp, i);
    }
    return primal;
}

double *lp_resolve (Matrix *m)
{
    glp_prob *lp;

    /* Create a solver */
    lp = glp_create_prob ();
    glp_set_prob_name (lp, "pl");
    glp_set_obj_dir (lp, GLP_MIN);

    /* Fill matrix informations */
    lp_add_row (lp, m);
    lp_add_column (lp, m);

    /* Load matrice */
    glp_load_matrix (lp, m->nbr_coefs, m->row_of_coef,
                     m->column_of_coef, m->matrix_coefs);

    /* Solve the lp */
    lp_mip_solve (lp);

    /* Save an array of primal and the obj solution. */
    double *primal = lp_extract_primal (lp, m);

    glp_delete_prob (lp);
    glp_free_env ();

    return primal;
}
