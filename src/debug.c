/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "debug.h"

void debug_taken_facility (Dataset *ds, bool *b)
{
    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        if (b[i])
        {
            printf ("%zu facility taken.\n", i);
        }
    }
}

void debug_matrix (Matrix *m)
{
    for (size_t i = 0; i <= m->nbr_coefs; i++)
    {
        printf ("[%d][%d] =\t %.0f\n", m->row_of_coef[i], m->column_of_coef[i], m->matrix_coefs[i]);
    }
}
