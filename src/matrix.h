/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "conf.h"
#include "evalparse.h"

typedef struct {
    int8_t *name;
    uint32_t nbr_rows; // Constraint count
    uint32_t nbr_columns;
    uint32_t nbr_coefs; // Coeff count
    double *matrix_coefs;
    int32_t *row_of_coef;
    int32_t *column_of_coef;
    double *obj_coefs;
    double *row_bounds;
    int32_t customer_count;
} Matrix;

Matrix *init_matrix (Dataset *ds);
void free_matrix (Matrix *m);
