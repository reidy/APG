/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "conf.h"

typedef struct
{
    char *filename;
    uint32_t nb_facility;
    uint32_t nb_customer;

    uint32_t **a_dataset;
} Dataset;

void free_dataset (Dataset *ds);
Dataset *dataset_parse (char *filename);
