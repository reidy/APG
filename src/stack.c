/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stack.h"

Stack *stack_init (void)
{
    return calloc (sizeof (Stack), 1);
}

uint32_t push (Stack *s, char *data)
{
    Element *e;
    
    if ((e = (Element *) malloc (sizeof (Element))) == NULL)
    {
        return -1;
    }
    
    if ((e->data = (char *) malloc (50 * sizeof (char))) == NULL)
    {
        return -1;
    }
    
    strcpy (e->data, data);
    e->next = s->start;
    s->start = e;
    s->size++;
    
    return 0;
}

uint32_t pop (Stack *s)
{
    Element *e;
    
    /* if the stack is empty: error */
    if (s->size == 0)
    {
        return -1;
    }
    
    e = s->start;
    s->start = s->start->next;
    free (e->data);
    free (e);
    s->size--;
    return 0;
}

void debug_stack (Stack *s)
{
    Element *e;
    
    e = s->start;
    
    for (size_t i = 0; i < s->size; i++)
    {
        printf("\t\t%s\n", e->data);
        e = e-> next;
    }
}

void free_stack (Stack *s)
{
    while (s->size > 0)
    {
        pop (s);
    }
    free (s);
}
