/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "evalparse.h"

Dataset *init_dataset (char *filename, int32_t nb_facility,
                       int32_t nb_customer)
{
    Dataset *ds = calloc (sizeof (Dataset), 1);

    ds->filename = malloc (sizeof (char) *
                            strlen ((const char *)filename) + 1);
    strcpy (ds->filename, filename);
    ds->nb_facility = nb_facility;
    ds->nb_customer = nb_customer;

    ds->a_dataset = malloc (sizeof (uint32_t *) * (nb_facility + 1));
    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        ds->a_dataset[i] = malloc (sizeof (uint32_t) *
                                    (nb_customer + 2));
    }

    printf ("Dataset created with n=%d and m=%d\n", ds->nb_facility,
                                                    ds->nb_customer);

    return ds;
}

void free_dataset (Dataset *ds)
{
    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        free (ds->a_dataset[i]);
    }
    free (ds->a_dataset);
    free (ds->filename);
    free (ds);
}

Dataset *dataset_parse (char *filename)
{
    FILE *f;

    if ((f = fopen (filename, "r")) == NULL)
    {
        perror ("File can't be opened.");
        exit (EXIT_FAILURE);
    }

    uint32_t nb_facility, nb_customer;
    char *s = malloc (sizeof (char) *
                      (strlen ((const char *)filename) + 1));
    fscanf (f, "FILE:%s\n", s);
    fscanf (f, "%u %u 0\n", &nb_facility, &nb_customer);

    Dataset *ds = init_dataset (s, nb_facility, nb_customer);
    uint8_t nothing;

    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        fscanf (f, "%c %u", &nothing, &ds->a_dataset[i][0]);
        for (size_t j = 1; j <= ds->nb_customer + 1; j++)
        {
            fscanf (f, "%u", &ds->a_dataset[i][j]);
        }
    }

    free (s);
    fclose (f);

    return ds;
}
