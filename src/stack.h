/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include "conf.h"

typedef struct ElementNext {
    char *data;
    struct ElementNext *next;
} Element;

typedef struct List {
    Element *start;
    uint32_t size;
} Stack;

Stack *stack_init ();
uint32_t push (Stack *s, char *data);
uint32_t pop (Stack *s);
void debug_stack (Stack *s);
void free_stack (Stack *s);
