/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "conf.h"
#include "evalparse.h"
#include "greedy.h"
#include "debug.h"
#include "lp.h"
#include "util.h"

int32_t main (int32_t U argc, char U **argv)
{
    Dataset *ds;

    if (argc < 2)
    {
        char s[100];
        sprintf (s, "Usage: %s [dataset]", argv[0]);
        perror (s);
        return EXIT_FAILURE;
    }

    ds = dataset_parse (argv[1]);

    /* Greedy Algorithm. */
    G1Result *g1 = greedy_one (ds);
    export_greedy1_to_file (g1, ds);
    greedy_one_free (g1);

    /* Linear Program solving. */
    Matrix *m = init_matrix (ds);
    double *primal = lp_resolve (m);
    export_primal_to_file (primal, m);

    free (primal);
    free_matrix (m);


    free_dataset (ds);

    return EXIT_SUCCESS;
}
