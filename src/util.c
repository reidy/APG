/*
 *  Copyright (C) 2016  Reid
 *
 *  This file is part of Approx Coverage
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "util.h"
#include <sys/stat.h>
#include <sys/types.h>
#define RESULT_FOLDER "./data/res/"
#define EPSILON 0.0000001

/* Export results to different files. */

void export_greedy1_to_file (G1Result *g1, Dataset *ds)
{
    char exp_filepath[100];
    strcpy (exp_filepath, RESULT_FOLDER);
    mkdir (exp_filepath, 0777);

    strcat (exp_filepath, (char *)ds->filename);
    strcat (exp_filepath, ".g1.opt");

    FILE *f;
    if ((f = fopen (exp_filepath, "w")) == NULL)
    {
        perror ("Export_greedy1_to_file: Can't create export file");
        exit (EXIT_FAILURE);
    }

    fprintf (f, "Greedy 1's best cost: %u\n", g1->cost);
    for (size_t i = 1; i <= ds->nb_facility; i++)
    {
        if (g1->open[i])
        {
            fprintf (f, "%zu facility taken.\n", i);
        }
    }

    printf ("Greedy 1's best cost: %u\n", g1->cost);

    printf ("Best cost and open facilities ID can be found on %s\n",
                                                          exp_filepath);

    fclose (f);
}

void export_primal_to_file (double *primal, Matrix *m)
{
    char exp_filepath[100];
    strcpy (exp_filepath, RESULT_FOLDER);
    mkdir (exp_filepath, 0777);

    strcat (exp_filepath, (char *)m->name);
    strcat (exp_filepath, ".lp.opt");

    FILE *f;
    if ((f = fopen (exp_filepath, "w")) == NULL)
    {
        perror ("Export_primal_to_file: Can't create export file");
        exit (EXIT_FAILURE);
    }

    fprintf (f, "Best solution objective value z = %lf\n", primal[0]);
    for (size_t i = 1; i <= m->nbr_columns; i++)
    {
        if (primal[i] < -EPSILON || primal[i] > EPSILON)
        {
            fprintf (f, "x_%zu = %lf\n", i, primal[i]);
        }
    }

    printf ("Best solution objective value z = %lf\n", primal[0]);

    printf ("Optimum and variable values can be found on %s\n",
                                                         exp_filepath);

    fclose (f);
}
