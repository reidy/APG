CC       = gcc
LIBS     = -lm -lglpk
CFLAGS   = -Wall -Wextra -std=c11 -pedantic -g
LDFLAGS  = -fsanitize=address -fsanitize=undefined -fsanitize=shift -fsanitize=integer-divide-by-zero -fsanitize=unreachable -fsanitize=vla-bound -fsanitize=null -fsanitize=return -fsanitize=signed-integer-overflow -fsanitize=bool -fsanitize=enum
RM       = rm -rf

HEADDIR  = ./src
SRCDIR   = ./src
OBJDIR   = ./obj
BINDIR   = ./

SOURCES  = $(wildcard $(SRCDIR)/*.c)
INCLUDES = $(wildcard $(HEADDIR)/*.h)
OBJECTS  = $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
BINARY = $(BINDIR)/main
ASANBIN = $(BINDIR)/test

all: $(BINARY)
$(BINARY): $(OBJECTS)
	@$(CC) $(LIBS) $^ -o $@
	@echo "Compiled" $@ "successfully."

asan: $(ASANBIN)
$(ASANBIN): $(OBJECTS)
	@$(CC) $(LIBS) $(LDFLAGS) $^ -o $@
	@echo "Compiled" $@ "successfully with libasan."

$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	@$(CC) $(CFLAGS) -c $< -o $@
	@echo "(CC) "$<""

clean:
	@$(RM) $(OBJECTS)
	@echo "Objects removed."

remove: clean
	@$(RM) $(BINDIR)/$(BINARY) $(BINDIR)/$(ASANBIN)
	@echo "Executable removed."

.PHONY: all clean remove
